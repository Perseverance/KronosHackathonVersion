var HCV = artifacts.require("./HashChainVerifier.sol");
var HCVO = artifacts.require("./DummyHashChainVerifierOwner.sol");
var w3 = require("web3-utils");

contract('HashChainVerifier', function(accounts) {

  let lottery, originalRevealer, challenger;

  const REVEALER_TURN = 1;
  const CHALLENGER_TURN = 2;

  // We will calculate the final balances of one contract here, so that we can migrate
  // them to a new contract.
  let instance;
  let ownerInstance, owner;
  let hashChainGood = [];
  let hashChainBad = [];  
  let descriptions = {};

  hashChainGood.push(w3.soliditySha3('foo'));
  hashChainBad.push(w3.soliditySha3('foo'));;
  

  for (let i = 1; i <= 5; i++) {
    hashChainGood.push(w3.soliditySha3(hashChainGood[i-1]));
    hashChainBad.push(w3.soliditySha3(hashChainBad[i-1]));    
    
    if (i == 4) {
        hashChainBad[i] = w3.soliditySha3('bar');
    }
    //console.log("New iteration");
    //console.log(hashChainGood);
    descriptions[hashChainGood[i]] = "Hash chain " + i + " is " + descriptions[hashChainGood[i]]; 
  }

  //console.log(hashChainGood[4]);
  //console.log(w3.soliditySha3(hashChainGood[4]));
  
  
  
  // Note that we could migrate allowances in the same way, but:
  // - A. this is arguably not necessary; allowances can be regranted
  // - B. we can do it but we'd need to add some functions to allow the owner to set
  //      allowances for any user (only before minting ends)
  
  describe("Failed challenge (proven on AGREEMENT)", async function() {
        
    before("should prepare", async function() {
        assert.isAtLeast(accounts.length, 4);
        lottery = accounts[0];
        originalRevealer = accounts[1];
        challenger = accounts[2];
        instance = await HCV.new({ from: lottery });
        ownerInstance = await HCVO.new({ from: lottery });
        owner = ownerInstance.address;
    });

    it("should have the expected owner", async function() {
        let own = await instance.owner({from: originalRevealer});
        assert.equal(own, lottery, "lottery wasn't in the owner");

        let state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[11].toNumber(), 0);
    });

    it("should update the owner", async function() {
        let ownTrx = instance.transferOwnership(owner, { from: lottery });

        own = await instance.owner({from: originalRevealer});
        assert.equal(own, owner, "owner wasn't in the owner");
    });

    it("should create new challenge", async function() {
        let retVal = await instance.challenge.call(hashChainGood[0],
                                                originalRevealer,
                                                hashChainGood[5],
                                                challenger,
                                                5,
                                                { from: lottery, value: 100 });
        assert.equal(retVal, true, "Can call challenge locally");

        let challengeTrx = await instance.challenge(hashChainGood[0],
                                                    originalRevealer,
                                                    hashChainGood[5],
                                                    challenger,
                                                    5,
                                                    { from: lottery, value: 100 });

        // Initial state                                            
        let state = await instance.state.call(hashChainGood[0]);
        //console.log(state);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 0); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[0]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[7], 2); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should process first claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to provide step 2
        await instance.claimStep(hashChainGood[0], state[7], hashChainGood[2], {from: originalRevealer});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 0); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[0]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[6], hashChainGood[2]); // latestClaim    
        assert.equal(state[7].toNumber(), 2); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should agree with first claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to agree or disagree with step 2
        // We agree with it
        await instance.respondToStep(hashChainGood[0], state[7], true, {from: challenger});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 2); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[2], "Expected " + descriptions[state[4]] + " but was " + descriptions[hashChainGood[2]]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[7].toNumber(), 3 ); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should process second claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to provide step 3
        await instance.claimStep(hashChainGood[0], state[7], hashChainGood[3], {from: originalRevealer});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[2], 2); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[2]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[6], hashChainGood[3]); // latestClaim    
        assert.equal(state[7].toNumber(), 3); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
    });

    it("should agree with second claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to  agree or disagree with step 3
        // We agree with it
        await instance.respondToStep(hashChainGood[0], state[7], true, {from: challenger});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[2], 3); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[3], "Expected " + descriptions[state[4]] + " but was " + descriptions[hashChainGood[2]]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[7].toNumber(), 4 ); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
    });

    it("should process third claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to provide step 4
        await instance.claimStep(hashChainGood[0], state[7], hashChainGood[4], {from: originalRevealer});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[2], 3); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[3]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[6], hashChainGood[4]); // latestClaim    
        assert.equal(state[7].toNumber(), 4); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
    });

    it("should AGREE with third claim, and be proven WRONG", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to  agree or disagree with step 4
        // We DISAGREE with it
        let respTrx = await instance.respondToStep(hashChainGood[0], state[7], true, {from: challenger});

        // Check that the originator can withdraw
        let withdrawTrx = await instance.withdrawPayments({from: originalRevealer});
    });

  });

  describe("Failed challenge (proven on DISAGREEMENT)", async function() {
        
    before("should prepare", async function() {
        assert.isAtLeast(accounts.length, 4);
        lottery = accounts[0];
        originalRevealer = accounts[1];
        challenger = accounts[2];
        instance = await HCV.new({ from: lottery });
        ownerInstance = await HCVO.new({ from: lottery });
        owner = ownerInstance.address;
    });

    it("should have the expected owner", async function() {
        let own = await instance.owner({from: originalRevealer});
        assert.equal(own, lottery, "lottery wasn't in the owner");

        let state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[11].toNumber(), 0);
    });

    it("should update the owner", async function() {
        let ownTrx = instance.transferOwnership(owner, { from: lottery });

        own = await instance.owner({from: originalRevealer});
        assert.equal(own, owner, "owner wasn't in the owner");
    });

    it("should create new challenge", async function() {
        let retVal = await instance.challenge.call(hashChainGood[0],
                                                originalRevealer,
                                                hashChainGood[5],
                                                challenger,
                                                5,
                                                { from: lottery, value: 100 });
        assert.equal(retVal, true, "Can call challenge locally");

        let challengeTrx = await instance.challenge(hashChainGood[0],
                                                    originalRevealer,
                                                    hashChainGood[5],
                                                    challenger,
                                                    5,
                                                    { from: lottery, value: 100 });

        // Initial state                                            
        let state = await instance.state.call(hashChainGood[0]);
        //console.log(state);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 0); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[0]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[7], 2); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should process first claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to provide step 2
        await instance.claimStep(hashChainGood[0], state[7], hashChainGood[2], {from: originalRevealer});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 0); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[0]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[6], hashChainGood[2]); // latestClaim    
        assert.equal(state[7].toNumber(), 2); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should agree with first claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to agree or disagree with step 2
        // We agree with it
        await instance.respondToStep(hashChainGood[0], state[7], true, {from: challenger});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 2); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[2], "Expected " + descriptions[state[4]] + " but was " + descriptions[hashChainGood[2]]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[7].toNumber(), 3 ); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should process second claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to provide step 3
        await instance.claimStep(hashChainGood[0], state[7], hashChainGood[3], {from: originalRevealer});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[2], 2); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[2]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[6], hashChainGood[3]); // latestClaim    
        assert.equal(state[7].toNumber(), 3); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
    });

    it("should agree with second claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to  agree or disagree with step 3
        // We agree with it
        await instance.respondToStep(hashChainGood[0], state[7], true, {from: challenger});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[2], 3); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[3], "Expected " + descriptions[state[4]] + " but was " + descriptions[hashChainGood[2]]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[7].toNumber(), 4 ); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
    });

    it("should process third claim", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to provide step 4
        await instance.claimStep(hashChainGood[0], state[7], hashChainGood[4], {from: originalRevealer});

        state = await instance.state.call(hashChainGood[0]);
        assert.equal(state[2], 3); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainGood[3]); // leftVal
        assert.equal(state[5], hashChainGood[5]); // rightVal
        assert.equal(state[6], hashChainGood[4]); // latestClaim    
        assert.equal(state[7].toNumber(), 4); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
    });

    it("should DISAGREE with third claim, and be proven WRONG", async function() {                                          
        let state = await instance.state.call(hashChainGood[0]);

        // Per nextStep = state[7], we need to  agree or disagree with step 4
        // We DISAGREE with it
        let respTrx = await instance.respondToStep(hashChainGood[0], state[7], false, {from: challenger});

        // Check that the originator can withdraw
        let withdrawTrx = await instance.withdrawPayments({from: originalRevealer});
    });

    });

  describe("Successful challenge", async function() {
    
    before("should prepare", async function() {
        assert.isAtLeast(accounts.length, 4);
        lottery = accounts[0];
        originalRevealer = accounts[1];
        challenger = accounts[2];
        instance = await HCV.new({ from: lottery });
        ownerInstance = await HCVO.new({ from: lottery });
        owner = ownerInstance.address;
    });

    it("should have the expected owner", async function() {
        let own = await instance.owner({from: originalRevealer});
        assert.equal(own, lottery, "lottery wasn't in the owner");

        let state = await instance.state.call(hashChainBad[0]);
        assert.equal(state[11].toNumber(), 0);
    });

    it("should update the owner", async function() {
        let ownTrx = instance.transferOwnership(owner, { from: lottery });

        own = await instance.owner({from: originalRevealer});
        assert.equal(own, owner, "owner wasn't in the owner");
    });

    it("should create new challenge", async function() {
        let retVal = await instance.challenge.call(hashChainBad[0],
                                                originalRevealer,
                                                hashChainBad[5],
                                                challenger,
                                                5,
                                                { from: lottery, value: 100 });
        assert.equal(retVal, true, "Can call challenge locally");

        let challengeTrx = await instance.challenge(hashChainBad[0],
                                                    originalRevealer,
                                                    hashChainBad[5],
                                                    challenger,
                                                    5,
                                                    { from: lottery, value: 100 });

        // Initial state                                            
        let state = await instance.state.call(hashChainBad[0]);
        //console.log(state);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 0); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainBad[0]); // leftVal
        assert.equal(state[5], hashChainBad[5]); // rightVal
        assert.equal(state[7], 2); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should process first claim", async function() {                                          
        let state = await instance.state.call(hashChainBad[0]);

        // Per nextStep = state[7], we need to provide step 2
        await instance.claimStep(hashChainBad[0], state[7], hashChainBad[2], {from: originalRevealer});

        state = await instance.state.call(hashChainBad[0]);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 0); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainBad[0]); // leftVal
        assert.equal(state[5], hashChainBad[5]); // rightVal
        assert.equal(state[6], hashChainBad[2]); // latestClaim    
        assert.equal(state[7].toNumber(), 2); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should agree with first claim", async function() {                                          
        let state = await instance.state.call(hashChainBad[0]);

        // Per nextStep = state[7], we need to agree or disagree with step 2
        // We agree with it
        await instance.respondToStep(hashChainBad[0], state[7], true, {from: challenger});

        state = await instance.state.call(hashChainBad[0]);
        assert.equal(state[0], originalRevealer); // claimant
        assert.equal(state[1], challenger); // challenger
        assert.equal(state[2], 2); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainBad[2], "Expected " + descriptions[state[4]] + " but was " + descriptions[hashChainBad[2]]); // leftVal
        assert.equal(state[5], hashChainBad[5]); // rightVal
        assert.equal(state[7].toNumber(), 3 ); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
        assert.equal(state[11].toNumber(), 100); // deposit
    });

    it("should process second claim", async function() {                                          
        let state = await instance.state.call(hashChainBad[0]);

        // Per nextStep = state[7], we need to provide step 3
        await instance.claimStep(hashChainBad[0], state[7], hashChainBad[3], {from: originalRevealer});

        state = await instance.state.call(hashChainBad[0]);
        assert.equal(state[2], 2); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainBad[2]); // leftVal
        assert.equal(state[5], hashChainBad[5]); // rightVal
        assert.equal(state[6], hashChainBad[3]); // latestClaim    
        assert.equal(state[7].toNumber(), 3); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
    });

    it("should agree with second claim", async function() {                                          
        let state = await instance.state.call(hashChainBad[0]);

        // Per nextStep = state[7], we need to  agree or disagree with step 3
        // We agree with it
        await instance.respondToStep(hashChainBad[0], state[7], true, {from: challenger});

        state = await instance.state.call(hashChainBad[0]);
        assert.equal(state[2], 3); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainBad[3], "Expected " + descriptions[state[4]] + " but was " + descriptions[hashChainBad[2]]); // leftVal
        assert.equal(state[5], hashChainBad[5]); // rightVal
        assert.equal(state[7].toNumber(), 4 ); // nextStep
        assert.equal(state[10].toNumber(), REVEALER_TURN); // turn
    });

    it("should process third claim", async function() {                                          
        let state = await instance.state.call(hashChainBad[0]);

        // Per nextStep = state[7], we need to provide step 4
        await instance.claimStep(hashChainBad[0], state[7], hashChainBad[4], {from: originalRevealer});

        state = await instance.state.call(hashChainBad[0]);
        assert.equal(state[2], 3); // left
        assert.equal(state[3], 5); // right
        assert.equal(state[4], hashChainBad[3]); // leftVal
        assert.equal(state[5], hashChainBad[5]); // rightVal
        assert.equal(state[6], hashChainBad[4]); // latestClaim    
        assert.equal(state[7].toNumber(), 4); // nextStep
        assert.equal(state[10].toNumber(), CHALLENGER_TURN); // turn
    });

    it("should disagree with third claim, and be proven RIGHT", async function() {                                          
        let state = await instance.state.call(hashChainBad[0]);

        // Per nextStep = state[7], we need to  agree or disagree with step 4
        // We DISAGREE with it
        let respTrx = await instance.respondToStep(hashChainBad[0], state[7], false, {from: challenger});

        // Check that the originator can withdraw
        let withdrawTrx = await instance.withdrawPayments({from: challenger});
    });

  });

});