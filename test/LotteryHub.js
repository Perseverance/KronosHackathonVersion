var LotteryHub = artifacts.require("./LotteryHub.sol");
const util = require('./util');
Promise = require("bluebird");
const expectThrow = util.expectThrow;

contract('LotteryHub', function (accounts) {
	let lotteryHubOwner,
		lotteryHubInstance,
		lotteryInstance;

	before("should prepare", function () {
		owner0 = accounts[0];
		participant0 = accounts[1];

	});


	let lottery_name = "a lottery name",
		lottery_steps = 1000,
		lottery_commitduration = 20,
		lottery_challengeduration = 20,
		lottery_challengefee = 100,
		lottery_seed = "0xca35b7d915458ef540ade6068dfe2f",
		lottery_price = 10000000;


	describe('LotteryHub/createLottery', function (accounts) {
		
		beforeEach("should prepare", async function () {
			lotteryHubInstance = await LotteryHub.new({ from: owner0, gas: 4000000 });
		});

		it('should create a new lottery', async function () {
			let tx = await lotteryHubInstance.createLottery(
				web3.fromUtf8(lottery_name), // bytes32 name, 
				lottery_steps, //uint _steps, 
				lottery_commitduration, //uint _commitDuration, 
				lottery_challengeduration, //uint _challengeDuration, 
				lottery_challengefee,// uint _challengeFee, 
				web3.fromUtf8(lottery_seed), //bytes32 seed, 
				{ from: participant0, value: lottery_price, gas: 4000000 }
			)


			assert.strictEqual(tx.receipt.logs.length, 1);
			assert.strictEqual(tx.logs.length, 1);
			const logLotteryCreated = tx.logs[0];
			assert.strictEqual(logLotteryCreated.event, "LogLotteryCreated");
			assert.strictEqual(web3.toUtf8(logLotteryCreated.args.name), lottery_name);
			assert.strictEqual(logLotteryCreated.args._steps.toNumber(), lottery_steps);
			assert.strictEqual(logLotteryCreated.args._commitDuration.toNumber(), lottery_commitduration);
			assert.strictEqual(logLotteryCreated.args._challengeDuration.toNumber(), lottery_challengeduration);
			assert.strictEqual(logLotteryCreated.args._challengeFee.toNumber(), lottery_challengefee);
			assert.strictEqual(web3.toUtf8(logLotteryCreated.args.seed), lottery_seed);
		});

	});
})
