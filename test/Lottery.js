var LotteryHub = artifacts.require("./LotteryHub.sol");
var Lottery = artifacts.require("./Lottery.sol");
const util = require('./util');
Promise = require("bluebird");
const expectThrow = util.expectThrow;

contract('Lottery', function (accounts) {
	let lotteryHubOwner,
		lotteryHubInstance,
		lotteryInstance;

	before("should prepare", async function () {
		lotteryHubOwner = accounts[0];
		participant0 = accounts[1];
		participant1 = accounts[2];
		participant2 = accounts[3];
		lotteryHubInstance = await LotteryHub.new({ from: lotteryHubOwner, gas: 4000000 });
	});


	let lottery_name = "a lottery name",
		lottery_steps = 1000,
		lottery_commitduration = 20,
		lottery_challengeduration = 20,
		lottery_challengefee = 100,
		lottery_seed = "0xca35b7d915458ef540ade6068dfe2f",
		lottery_price = 10000000,

		lottery_seed1 = "0xca35b7d913458ef540ade6068dfe2f",
		lottery_seed2 = "0xca35c7d915458ef540ade6068dfe2f";



	describe('Lottery', function (accounts) {
		let lotteryInstance;
		beforeEach("should prepare", async function () {
			lotteryInstance = await Lottery.new(
				participant0,
				lottery_steps, //uint _steps, 
				lottery_commitduration, //uint _commitDuration, 
				lottery_challengeduration, //uint _challengeDuration, 
				lottery_challengefee,// uint _challengeFee, 
				web3.fromUtf8(lottery_seed), //bytes32 seed, 
				{ from: participant0, value: lottery_price, gas: 4000000 }
			)
		});

		it('should submit a commit', async function () {

			tx = await lotteryInstance.submit(web3.fromUtf8(lottery_seed1),
				{
					from: participant1,
					value: lottery_price,
					gas: 4000000
				});

			assert.strictEqual(tx.receipt.logs.length, 1);
			assert.strictEqual(tx.logs.length, 1);
			const logCommitSubmitted = tx.logs[0];
			assert.strictEqual(logCommitSubmitted.event, "LogCommitSubmitted");
			assert.strictEqual(web3.toUtf8(logCommitSubmitted.args.seed), lottery_seed1);

		});


		it('should advance to Revealing stage after 2 commits', async function () {

			tx = await lotteryInstance.submit(web3.fromUtf8(lottery_seed1),
				{
					from: participant1,
					value: lottery_price,
					gas: 2000000
				});

			assert.strictEqual(tx.receipt.logs.length, 1);
			assert.strictEqual(tx.logs.length, 1);
			const logCommitSubmitted = tx.logs[0];
			assert.strictEqual(logCommitSubmitted.event, "LogCommitSubmitted");
			assert.strictEqual(web3.toUtf8(logCommitSubmitted.args.seed), lottery_seed1);

			tx2 = await lotteryInstance.submit(web3.fromUtf8(lottery_seed2),
				{
					from: participant2,
					value: lottery_price,
					gas: 2000000
				});

			assert.strictEqual(tx2.receipt.logs.length, 2);
			assert.strictEqual(tx2.logs.length, 2);
			const logCommitSubmitted2 = tx2.logs[0];
			assert.strictEqual(logCommitSubmitted2.event, "LogCommitSubmitted");
			assert.strictEqual(web3.toUtf8(logCommitSubmitted2.args.seed), lottery_seed2);

			const logCommitSubmitted3 = tx2.logs[1];
			assert.strictEqual(logCommitSubmitted3.event, "LogStageAdvance");
			assert.strictEqual(logCommitSubmitted3.args.newStage.toNumber(), 1);

		})

		it('should accept a reveals in Revealing stage', async function () {

			tx = await lotteryInstance.submit(web3.fromUtf8(lottery_seed1),
				{
					from: participant1,
					value: lottery_price,
					gas: 2000000
				});

			assert.strictEqual(tx.receipt.logs.length, 1);
			assert.strictEqual(tx.logs.length, 1);
			const logCommitSubmitted = tx.logs[0];
			assert.strictEqual(logCommitSubmitted.event, "LogCommitSubmitted");
			assert.strictEqual(web3.toUtf8(logCommitSubmitted.args.seed), lottery_seed1);

			tx2 = await lotteryInstance.submit(web3.fromUtf8(lottery_seed2),
				{
					from: participant2,
					value: lottery_price,
					gas: 2000000
				});

			assert.strictEqual(tx2.receipt.logs.length, 2);
			assert.strictEqual(tx2.logs.length, 2);
			const logCommitSubmitted2 = tx2.logs[0];
			assert.strictEqual(logCommitSubmitted2.event, "LogCommitSubmitted");
			assert.strictEqual(web3.toUtf8(logCommitSubmitted2.args.seed), lottery_seed2);

			const logCommitSubmitted3 = tx2.logs[1];
			assert.strictEqual(logCommitSubmitted3.event, "LogStageAdvance");
			assert.strictEqual(logCommitSubmitted3.args.newStage.toNumber(), 1);

			let reveal0 = web3.sha3(lottery_seed).substring(0,32);

			tx3 = await lotteryInstance.reveal(participant0, web3.fromUtf8(reveal0));
			
			assert.strictEqual(tx3.receipt.logs.length, 1);
			assert.strictEqual(tx3.logs.length, 1);
			const logReveal = tx3.logs[0];
			assert.strictEqual(logReveal.event, "LogReveal");
			assert.strictEqual(logReveal.args.committer, participant0);
			assert.strictEqual(web3.toUtf8(logReveal.args.reveal), reveal0);

		})


        /* xit('should create a new lottery', async function () {



            assert.strictEqual(tx.receipt.logs.length, 1);
            assert.strictEqual(tx.logs.length, 1);
            const logLotteryCreated = tx.logs[0];
            assert.strictEqual(logLotteryCreated.event, "LogLotteryCreated");
            assert.strictEqual(web3.toUtf8(logLotteryCreated.args.name), lottery_name);
            assert.strictEqual(logLotteryCreated.args._steps.toNumber(), lottery_steps);
            assert.strictEqual(logLotteryCreated.args._commitDuration.toNumber(), lottery_commitduration);
            assert.strictEqual(logLotteryCreated.args._challengeDuration.toNumber(), lottery_challengeduration);
            assert.strictEqual(logLotteryCreated.args._challengeFee.toNumber(), lottery_challengefee);
            assert.strictEqual(web3.toUtf8(logLotteryCreated.args.seed), lottery_seed);
        }); */

	});
})
