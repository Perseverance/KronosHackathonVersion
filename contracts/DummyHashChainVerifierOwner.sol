pragma solidity ^0.4.15;

contract DummyHashChainVerifierOwner {

    event LogChallengeDecisionReceived(bytes32 originalUnhashedString, bool challengeWasSuccessful);

    function challengeDecided(bytes32 originalUnhashedString, bool challengeWasSuccessful)
    {
        LogChallengeDecisionReceived(originalUnhashedString, challengeWasSuccessful);
    }
}