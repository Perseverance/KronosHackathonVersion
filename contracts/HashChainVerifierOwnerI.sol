pragma solidity ^0.4.15;

contract HashChainVerifierOwnerI {

    function challengeDecided(bytes32 originalUnhashedString, bool challengeWasSuccessful);

}
