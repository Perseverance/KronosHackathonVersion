pragma solidity ^0.4.13;

import './Lottery.sol';

contract LotteryHub {

  event LogLotteryCreated(address addr, bytes32 name, uint price, uint _steps, uint _commitDuration, uint _challengeDuration, uint _challengeFee, bytes32 seed);

  function LotteryHub () {}

  function createLottery(bytes32 name, uint _steps, uint _commitDuration, uint _challengeDuration, uint _challengeFee, bytes32 seed) payable {
    Lottery lottery = (new Lottery).value(msg.value)(msg.sender, _steps, _commitDuration, _challengeDuration, _challengeFee, seed);
    LogLotteryCreated(lottery, name, lottery.price(), _steps, _commitDuration, _challengeDuration, _challengeFee, seed);
  }
}
