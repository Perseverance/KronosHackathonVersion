pragma solidity ^0.4.13;

import "./HashChainVerifier.sol";
import "./HashChainVerifierOwnerI.sol";

contract Lottery is HashChainVerifierOwnerI {
  uint public steps;
  uint public commitDeadline;
  uint public price;
  HashChainVerifier public trustedChallengeContract;
  uint public challengeDuration;
  uint public challengeDeadline;
  uint public challengeFee;
  bytes32 public challengeProposal;
  address public challenger;
  address public challengeDefender;

  uint revealsMade;

  address[] public committers;
  mapping(address => bytes32) public commits;
  mapping(address => bytes32) public reveals;

  mapping(address => bool) public disqualified;

  event LogCommitSubmitted(bytes32 seed);
  event LogStageAdvance(Stages newStage);
  event LogReveal(address committer, bytes32 reveal);
  event LogChallengeEnded(bytes32 originalUnhashedString, bool challengeWasSuccessful);

  enum Stages {
    Committing,
    Revealing,
    WaitingForChallenge,
    PausedForChallenge,
    Finished
  }

  Stages public currentStage;

  modifier atStage(Stages _stage) {
    require(currentStage == _stage);
    _;
  }

  function nextStage() {
    currentStage = Stages(uint(currentStage) + 1);

	LogStageAdvance(currentStage);
  }

  function commit(address committer, bytes32 seed) internal {
    commits[committer] = seed;
    committers.push(committer);
    if (committers.length == 3) nextStage();
  }

  modifier isValidCommit(bytes32 seed) {
    require(seed != 0);
    require(msg.value > 0);
    _;
  }

  function Lottery(address committer, uint _steps, uint _commitDuration, uint _challengeDuration, uint _challengeFee, bytes32 seed) 
    public 
    payable 
    isValidCommit(seed) {

    price = msg.value;
    steps = _steps;

    commitDeadline = block.number + _commitDuration;

    trustedChallengeContract = new HashChainVerifier();
    challengeDuration = _challengeDuration;
    challengeFee = _challengeFee;

    commit(committer, seed);
  }

  function submit(bytes32 seed) 
    public 
    payable 
    isValidCommit(seed)
    atStage(Stages.Committing) {
      require(block.number <= commitDeadline);
      require(msg.value == price);
      LogCommitSubmitted(seed);
      commit(msg.sender, seed);
  }

  function withdrawRefund() public atStage(Stages.Committing){
    require(block.number > commitDeadline);
    require(commits[msg.sender] != 0);
    delete commits[msg.sender];
    msg.sender.transfer(price);
  }

  function reveal(address committer, bytes32 _reveal) 
    public
    atStage(Stages.Revealing) {

    require(commits[committer] != 0);
    require(reveals[committer] == 0);

    reveals[committer] = _reveal;
    revealsMade++;
	LogReveal(committer, _reveal);
    
    if (revealsMade == 3) {
      challengeDeadline = block.number + challengeDuration;
      nextStage();
    }
  }

  function challenge(address committer, bytes32 proposal) 
    public
    payable
    atStage(Stages.WaitingForChallenge) {
    require(challengeFee == msg.value);
    require(block.number <= challengeDeadline);
    require(commits[committer] != 0);
    require(commits[msg.sender] != 0);

    challenger = msg.sender;
    challengeProposal = proposal;
    challengeDefender = committer;

    trustedChallengeContract.challenge.value(challengeFee)(
      commits[committer],
      committer,
      reveals[committer],
      msg.sender,
      steps
    );

    nextStage();
  }

  // Called by challenge contract
  function challengeDecided(bytes32 originalUnhashedString, bool challengeWasSuccessful)
    atStage(Stages.PausedForChallenge) {

      // TODO: limit challenges?
      if (challengeWasSuccessful) {
        reveals[challengeDefender] = challengeProposal;
        disqualified[challengeDefender] = true;
      } else {
        disqualified[challenger] = true;
      }

      challengeDeadline = block.number + challengeDuration;
      currentStage = Stages.WaitingForChallenge;
      LogChallengeEnded(originalUnhashedString, challengeWasSuccessful);
  }

  function endLottery()
    public
    atStage(Stages.WaitingForChallenge) {
    require(block.number > challengeDeadline);
    address winner = getWinner();
    if (disqualified[winner]) {
      // TODO: BACKDOORRRRRRRRRR
      selfdestruct(address(0));
    } else {
      selfdestruct(winner);
    }
  }

  function getWinner() constant public returns(address) {
    bytes32 winner = reveals[committers[0]];
    for (uint i = 1; i < 3; i++) {
      winner ^= reveals[committers[i]];
    }
    return committers[uint(winner) % 3];
  }

}
