pragma solidity ^0.4.15;

import 'zeppelin-solidity/contracts/ownership/Ownable.sol';
import 'zeppelin-solidity/contracts/payment/PullPayment.sol';
import './HashChainVerifierOwnerI.sol';

// @TODO (if continuing after hackathon):
// * Multiple challengers (proposer should not win just because one challenger fails)
// * Require "gas available" proof for timeout
// * Charge gas costs of winner to the loser
/**
  * @title Challenge-Response verifier for a chain of keccack256 hashes.
  *
  * Summary
  * -------
  *
  * The supported scenario is proving or disproving a claimed off-chain calculation of
  * keccack256^n(input) for any input and any positive integer n. Calling #challenge
  * initiates a challenge-response protocol, whereby a challenger pays a deposit
  * to challenge a value previously claimed by the claimant.
  *
  * We then  do a binary search
  * to find the specific hash on which the original claimant and the challenger
  * disagree, then we calculate that hash on-chain to either uphold or reject the challenge.
  *
  * If the challenge is successful, they get their deposit back. If it fails, their deposit
  * is paid to the original submitter. Either way, the callback #owner.challengeDecided
  * is invoked to communicate the result.
  *
  * Detailed usage
  * --------------
  *
  * Only one concurrent challenge is supported for any given input. This must complete
  * before any new challenge can be initiated. Note that neither a successful nor a failed
  * challenge proves any claimed result (the challenger could be a stooge), so in both cases
  * the claimed value should remain challengeable.
  *
  * We implement no deterrent to repeated challenges of ones own value, other than the gas
  * cost of challenging oneself. The owner may wish to implement a stronger deterrant.
  *
  * The entry point for a new challenge is #challenge. After #challenge has been called,
  * the address claiming the original response must
  * call #claimStep to submit their claimed value for step #state.nextStep (i.e.
  * keccack256^nextStep(input), where nextStep will be the mid-point of the chain of hashes).
  * The challenger must then call #respondToStep for the same
  * #state.nextStep, to either agree or disagree with the proposed value. This updates
  * #step.nextStep and it is the challenger's turn again, for some new mid-point of a
  * smaller search space.
  *
  * Eventually, we find two consecutive steps values, where the left/earlier value is agreed upon
  * and the right/later value generates disagreement. We evaluate "keccack256(left) == right"
  * to either uphold or reject the challenge.
  * 
  * @author Neil McLaren
  */
contract HashChainVerifier is Ownable, PullPayment {

    uint public constant RESPONSE_TIME = 1 hours;
    
    enum NextTurnChoices {NoNextTurn, ClaimantsTurn, ChallengersTurn}
    
    /**
     * Called (e.g. by owning contract) when the result of a chain of consecutive hashes
     * is disupted).
     *
     * This is the entry point for a 
     * @author Neil McLaren
     */
    function challenge(bytes32 originalUnhashedString,
                       address _claimant,
                       bytes32 _claimReveal,
                       address _challenger,
                       uint    _roundsOfHashing)
        external
        payable
        returns(bool)
    {
        // Cannot challenge a given commitment if a challenge is already in progress for it
        require(state[originalUnhashedString].claimant == address(0));
        require(msg.value > 0);

        // Require meaningful params
        require(_claimant != address(0));
        require(_challenger != address(0));
        require(_claimReveal != bytes32(0));

        state[originalUnhashedString] = VerificationSession({
            claimant: _claimant,
            challenger: _challenger,
            left: 0,
            leftVal: originalUnhashedString,
            right: _roundsOfHashing,
            rightVal: _claimReveal,
            latestClaim: bytes32(0),
            nextStep: _roundsOfHashing / 2,
            lastClaimantMessage: now,
            lastChallengerMessage: now,
            turn: NextTurnChoices.ClaimantsTurn,
            challengerBondPaidWei: msg.value
        });

        LogNewChallenge(originalUnhashedString, _claimant, _claimReveal, _challenger, _roundsOfHashing);
        return true;
    }

    struct VerificationSession {
        address claimant;
        address challenger;
        uint left; // The highest index X such that there is known agreement on keccack256^X(c)
        uint right; // The lowest index X such that there is known disagreement on keccack256^X(c)
        bytes32 leftVal; // keccack256^left(c), as agreed
        bytes32 rightVal; // keccack256^right(c), as claimed by the original claimant but disputed by the challenger
        bytes32 latestClaim;
        uint nextStep; // (left+right)/2
        uint lastClaimantMessage; // timestamp
        uint lastChallengerMessage; // timestamp
        NextTurnChoices turn;
        uint challengerBondPaidWei;
    }

    mapping(bytes32 => VerificationSession) public state;

    event LogNewChallenge(bytes32 _commitment, address _claimant, bytes32 _claimReveal, address _challenger, uint    _roundsOfHashing);
    event LogNewStepClaimed(bytes32 _commitment, uint _step, bytes32 _stepResult);
    event LogNewResponseFromChallenger(bytes32 _commitment, uint _step, bool _agreeWithClaimant);
    event LogChallengerConvicted(bytes32 _commitment);
    event LogClaimantConvicted(bytes32 _commitment);

    modifier onlyClaimant(bytes32 _commitment) {
        require(msg.sender == state[_commitment].claimant);
        require(state[_commitment].turn == NextTurnChoices.ClaimantsTurn);
        _;
    }

    modifier onlyChallenger(bytes32 _commitment) {
        require(msg.sender == state[_commitment].challenger);
        require(state[_commitment].turn == NextTurnChoices.ChallengersTurn);
        _;
    }

    function claimStep(bytes32 _commitment, uint _step, bytes32 _stepResult)
        external
        onlyClaimant(_commitment)
        returns(bool)
    {
        VerificationSession storage s = state[_commitment];
        require(_step == s.nextStep);
        require(s.latestClaim == bytes32(0)); // Cannot claim same step twice
        s.latestClaim = _stepResult; 
        LogNewStepClaimed(_commitment, _step, _stepResult);    
        state[_commitment].lastClaimantMessage = now;
        state[_commitment].turn = NextTurnChoices.ChallengersTurn;
        return true;
    }

    function respondToStep(bytes32 _commitment, uint _step, bool _agreeWithClaimant)
        external
        onlyChallenger(_commitment)
        returns(bool)
    {
        VerificationSession storage s = state[_commitment];
        require(_step == s.nextStep);
        require(s.latestClaim != bytes32(0));

        if (_agreeWithClaimant) {
            // Move our window to the right
            s.left = s.nextStep;
            s.leftVal = s.latestClaim;
        } else {
            // Move our window to the left
            s.right = s.nextStep;
            s.rightVal = s.latestClaim;
        }

        // Check to see if we are finished
        if (s.left + 1 >= s.right) {
            // We have found the step on which there is disagreement
            // Who is correct?
            if (keccak256(s.leftVal) == s.rightVal) {
                // The claimant is correct
                challengeFailed(_commitment);
            } else {
                // The challenger is correct
                challengeSucceeded(_commitment);
            }
        }
        else
        {
            s.nextStep = (s.left + s.right) / 2;
            s.latestClaim = bytes32(0);
        }

        LogNewResponseFromChallenger(_commitment, _step, _agreeWithClaimant);
        state[_commitment].lastChallengerMessage = now;
        state[_commitment].turn = NextTurnChoices.ClaimantsTurn;
        return true;
    }

    function timeout(bytes32 commitment)
        returns (bool challengeHasTimedOut)
    {
        VerificationSession storage s = state[commitment];

        require(s.claimant != 0);
        if (s.turn == NextTurnChoices.ClaimantsTurn &&
            now > s.lastChallengerMessage + RESPONSE_TIME)
        {
            assert(s.lastChallengerMessage >= s.lastClaimantMessage);
            challengeSucceeded(commitment);
            return true;
        }           
        else if (s.turn == NextTurnChoices.ChallengersTurn &&
                 now > s.lastClaimantMessage + RESPONSE_TIME)
        {
            assert(s.lastClaimantMessage > s.lastChallengerMessage);
            challengeFailed(commitment);
            return true;
        }       
        else
        {
            // We have not timed out
            return false;
        }          
    }
    
    function challengeFailed(bytes32 commitment) internal {
        VerificationSession storage s = state[commitment];
        address depositSentTo = s.claimant;
        uint depositValue = s.challengerBondPaidWei;
        delete state[commitment];
        asyncSend(depositSentTo, depositValue);
        LogChallengerConvicted(commitment);
        HashChainVerifierOwnerI(owner).challengeDecided(commitment, false); // TODO: earlier detection of owner that does not satisfy interface
    }

    function challengeSucceeded(bytes32 commitment) internal {
        VerificationSession storage s = state[commitment];
        address depositSentTo = s.challenger;
        uint depositValue = s.challengerBondPaidWei;
        delete state[commitment];
        asyncSend(depositSentTo, depositValue);
        LogClaimantConvicted(commitment);
        HashChainVerifierOwnerI(owner).challengeDecided(commitment, true); // TODO: earlier detection of owner that does not satisfy interface
    }
}
