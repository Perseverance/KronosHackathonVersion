import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { AtfComponent } from './atf/atf.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HeaderComponent, AtfComponent],
  exports: [HeaderComponent, AtfComponent]
})
export class CoreModule { }
