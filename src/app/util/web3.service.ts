import { NextObserver } from 'rxjs/Observer';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Injectable, OnInit, Output, EventEmitter } from '@angular/core';
import { default as Web3 } from 'web3';
import { default as contract } from 'truffle-contract';
import { WindowRefService } from './window-ref.service';
import lotteryHub_artifact from '../../../build/contracts/LotteryHub.json';
import lottery_artifact from '../../../build/contracts/Lottery.json';

@Injectable()
export class Web3Service {


	private _web3: Web3;
	public LotteryHubContract: any;
	public metamaskUser: string;

	private userAcountStored: ReplaySubject<string>;

	constructor(private windowRef: WindowRefService) {
		this.setupLotteryHubContract();
		this.userAcountStored = new ReplaySubject<string>(1);
	}

	private setupLotteryHubContract() {
		this.setupMetamask_Web3();
		this.LotteryHubContract = contract(lotteryHub_artifact);
		this.LotteryHubContract.setProvider(this._web3.currentProvider);
	}

	private setupMetamask_Web3() {
		if (!this.windowRef.nativeWindow) {
			throw new Error('Can not get the window');
		}
		if (!this.windowRef.nativeWindow.web3) {
			throw new Error('Not a metamask browser');
		}
		this._web3 = new Web3(this.windowRef.nativeWindow.web3.currentProvider);
	}

	public async getLotteryContractInstance(address: string): Promise<any> {
		const LotteryContract = contract(lottery_artifact);
		LotteryContract.setProvider(this._web3.currentProvider);
		return await LotteryContract.at(address);
	}

	private pushUserStored(userAccount: string) {
		this.userAcountStored.next(userAccount);
	}

	public subscribeToUserStored(observer: NextObserver<string>) {
		this.userAcountStored.subscribe(observer);
	}

	public async storeMetamaskUserAccount() {
		const result = await this._web3.eth.getAccounts();
		this.metamaskUser = result[0];
		this.pushUserStored(this.metamaskUser);
	}

	public hashChain(seed: string, steps: number): string {
		let result = seed;
		for (let i = 0; i < steps; i++) {
			result = this._web3.utils.soliditySha3(result);
		}
		return result;
	}

	public get utils(): any {
		return this._web3.utils;
	}

	public get eth(): any {
		return this._web3.eth;
	}

	public get web3(): any {
		return this._web3;
	}

}
