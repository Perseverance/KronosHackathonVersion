import { ChallengeService, LotteryEvent } from './../challenge.service';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-challenges-list',
	templateUrl: './challenges-list.component.html',
	styleUrls: ['./challenges-list.component.scss']
})
export class ChallengesListComponent implements OnInit {

	private userAccount: string;

	constructor(private challengeService: ChallengeService) {
		this.challengeService.subscribeToUserStored({
			next: (account: string) => {
				this.userAccount = account;
			}
		});

		this.challengeService.subscribeToLotteryEvents({
			next: (account: LotteryEvent) => {
				console.log(account);
			}
		});

		this.challengeService.startListeningForEvents();
	}

	ngOnInit() {

	}

}
