import { LotteryEvent } from './challenge.service';
import { Subject } from 'rxjs/Subject';
import { NextObserver, Observer } from 'rxjs/Observer';
import { Web3Service } from './../util/web3.service';
import { Injectable } from '@angular/core';

export interface LotteryEvent {
	lotteryAddress: string;
	priceInEth: number;
}

@Injectable()
export class ChallengeService {

	public joinedLotteries = {};

	public revealSubject: Subject<any>;
	public completeSubject: Subject<any>;
	public challengesSubject: Subject<LotteryEvent>;

	constructor(private web3Service: Web3Service) {
		this.web3Service.storeMetamaskUserAccount();
		this.challengesSubject = new Subject<LotteryEvent>();
		this.revealSubject = new Subject<any>();
		this.completeSubject = new Subject<any>();
	}

	public async createLottery(priceInEth: number, lotteryName: string, fromUser: string): Promise<LotteryEvent> {
		const lotteryHubInstance = await this.web3Service.LotteryHubContract.deployed();
		const random32ByteHex = this.web3Service.utils.randomHex(32);
		const priceInWei = this.web3Service.utils.toWei(priceInEth, 'ether');

		const steps = 10000;
		const commitDuration = 10;
		const challengeDuration = 2000;
		const challengeFee = 100000;
		const res = await lotteryHubInstance.createLottery(
			lotteryName,
			steps,
			commitDuration,
			challengeDuration,
			challengeFee,
			random32ByteHex,
			{
				gas: 4000000,
				from: fromUser,
				value: priceInWei
			}
		);
		const response = res.logs[0];
		const lottery = { lotteryAddress: response.args.addr, priceInEth: response.args.price.toString(10) };
		this.joinedLotteries[lottery.lotteryAddress] = lottery;
		return lottery;
	}

	public async startListeningForEvents() {
		const self = this;
		const lotteryHubInstance = await this.web3Service.LotteryHubContract.deployed();
		const watcher = lotteryHubInstance.LogLotteryCreated({}, { fromBlock: 0, toBlock: 'latest' });
		watcher.watch(async (err, response) => {
			const lotteryInstance = await this.web3Service.getLotteryContractInstance(response.args.addr);
			const deadline = await lotteryInstance.commitDeadline.call();
			this.web3Service.eth.getBalance();
			const result = {
				lotteryAddress: response.args.addr,
				lotteryName: this.web3Service.utils.hexToAscii(response.args.name),
				priceInEth: this.web3Service.utils.fromWei(response.args.price.toString(10)),
				challengeDeadlineBlock: deadline
			};
			console.log(result);
			// this.pushLotteryEvent(response);
		});
	}

	private pushLotteryEvent(event: LotteryEvent) {
		this.challengesSubject.next(event);
	}

	public subscribeToUserStored(observer: NextObserver<string>) {
		return this.web3Service.subscribeToUserStored(observer);
	}

	public subscribeToLotteryEvents(observer: NextObserver<LotteryEvent>) {
		return this.challengesSubject.subscribe(observer);
	}

	public async joinLottery(address: string, fromUser: string) {
		const lotteryInstance = await this.web3Service.getLotteryContractInstance(address);
		const random32ByteHex = this.web3Service.utils.randomHex(32);
		const deadline = await lotteryInstance.commitDeadline.call();
		const price = await lotteryInstance.price.call();
		const res = await lotteryInstance.submit(random32ByteHex, { from: fromUser, gas: 1500000, value: price });
	}

	public async startListeningForReveal(address: string, fromUser: string, observer: Observer<any>) {
		const SECONDS_TO_REVEAL = 20;
		this.revealSubject.subscribe(observer);
		const self = this;
		const lotteryInstance = await this.web3Service.getLotteryContractInstance(address);
		let intervalcount = 0;
		const timeout = setInterval(async function () {
			intervalcount++;
			self.revealSubject.next(intervalcount / SECONDS_TO_REVEAL);
			if (intervalcount === SECONDS_TO_REVEAL) {
				self.revealSubject.complete();
				clearInterval(timeout);
			}
		}, 1000);
	}

	public async revealSecret(address: string, fromUser: string) {
		const lotteryInstance = await this.web3Service.getLotteryContractInstance(address);
		const random32ByteHex = this.web3Service.utils.randomHex(32);
		const res = await lotteryInstance.reveal(fromUser, random32ByteHex, { from: fromUser, gas: 1500000 });
		const stage = await lotteryInstance.currentStage({});
		console.log(stage);
	}

	public async startListeningForComplete(contractAddress: string, fromUser: string, observer: Observer<any>) {
		const SECONDS_TO_COMPLETE = 20;
		this.completeSubject.subscribe(observer);
		const self = this;
		const lotteryInstance = await this.web3Service.getLotteryContractInstance(contractAddress);
		let intervalcount = 0;
		const timeout = setInterval(async function () {
			intervalcount++;
			self.revealSubject.next(intervalcount / SECONDS_TO_COMPLETE);
			if (intervalcount === SECONDS_TO_COMPLETE) {
				self.completeSubject.complete();
				clearInterval(timeout);
			}
		}, 1000);
	}

	public async getWinner(contractAddress: string): Promise<string> {
		const lotteryInstance = await this.web3Service.getLotteryContractInstance(contractAddress);
		const winner = await lotteryInstance.getWinner();
		return winner.toLowerCase();
	}

	public async claimReward(contractAddress: string, fromUser: string) {
		const lotteryInstance = await this.web3Service.getLotteryContractInstance(contractAddress);
		await lotteryInstance.getPrize({ from: fromUser, gas: 1500000 });
		return true;
	}

}
