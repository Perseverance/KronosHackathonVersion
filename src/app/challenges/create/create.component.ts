import { ChallengeService, LotteryEvent } from './../challenge.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgProgressService } from 'ngx-progressbar';

@Component({
	selector: 'app-create-challenge',
	templateUrl: './create.component.html',
	styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

	public createChallengeForm: FormGroup;
	public userAccount: string;
	public lotteryAddress: string;
	public revealStage = false;
	public lotteryWinner = null;
	public rewardClaimed = false;

	constructor(private formBuilder: FormBuilder, private challengeService: ChallengeService, private progressService: NgProgressService) {
		this.createChallengeForm = this.formBuilder.group({
			challengePrice: [null, [Validators.required]],
			challengeName: [null, [Validators.required]]
		});

		this.challengeService.subscribeToUserStored({
			next: (account: string) => {
				this.userAccount = account.toLowerCase();
			}
		});

		this.progressService.start();
		console.log(this.progressService.isStarted());

	}

	ngOnInit() {
	}

	public get challengePrice() {
		return this.createChallengeForm.get('challengePrice');
	}

	public get challengeName() {
		return this.createChallengeForm.get('challengeName');
	}

	public async createLottery() {
		const self = this;
		const lottery = await this.challengeService.createLottery(this.challengePrice.value, this.challengeName.value, this.userAccount);
		this.lotteryAddress = lottery.lotteryAddress;
		await this.challengeService.startListeningForReveal(this.lotteryAddress, this.userAccount, {
			complete: () => {
				self.revealStage = true;
				console.log(self.revealStage);
			},
			next: (progress: number) => {
				console.log(progress);
			},
			error: () => {

			}
		});
	}

	public async reveal() {
		await this.challengeService.startListeningForComplete(this.lotteryAddress, this.userAccount, {
			complete: async () => {
				console.log('Get the winner');
				const winner = await this.challengeService.getWinner(this.lotteryAddress);
				this.lotteryWinner = winner;
			},
			next: (progress: number) => {
			},
			error: () => {

			}
		});
		await this.challengeService.revealSecret(this.lotteryAddress, this.userAccount);
	}

	public async claim() {
		this.rewardClaimed = await this.challengeService.claimReward(this.lotteryAddress, this.userAccount);
	}

}
