import { NgProgressService } from 'ngx-progressbar';
import { ChallengeService } from './../challenge.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-join',
	templateUrl: './join.component.html',
	styleUrls: ['./join.component.scss']
})
export class JoinComponent implements OnInit {

	public joinForm: FormGroup;

	public userAccount: string;
	public revealStage: boolean;
	public lotteryWinner = null;
	public rewardClaimed = false;

	constructor(private formBuilder: FormBuilder, private challengeService: ChallengeService, private progressService: NgProgressService) {

		this.joinForm = this.formBuilder.group({
			challengeAddress: [null, [Validators.required]]
		});

		this.challengeService.subscribeToUserStored({
			next: (account: string) => {
				this.userAccount = account.toLowerCase();
			}
		});
	}

	ngOnInit() {
	}

	public get challengeAddress() {
		return this.joinForm.get('challengeAddress');
	}

	public async joinLottery() {
		await this.challengeService.startListeningForReveal(this.challengeAddress.value, this.userAccount, {
			complete: () => {
				this.revealStage = true;
				console.log(this.revealStage);
			},
			next: (progress: number) => {
				console.log(progress);
			},
			error: () => {

			}
		});
		await this.challengeService.joinLottery(this.challengeAddress.value, this.userAccount);
	}

	public async reveal() {
		await this.challengeService.startListeningForComplete(this.challengeAddress.value, this.userAccount, {
			complete: async () => {
				console.log('Get the winner');
				const winner = await this.challengeService.getWinner(this.challengeAddress.value);
				this.lotteryWinner = winner;
				console.log(winner);
			},
			next: (progress: number) => {
			},
			error: () => {

			}
		});
		await this.challengeService.revealSecret(this.challengeAddress.value, this.userAccount);
	}

	public async claim() {
		this.rewardClaimed = await this.challengeService.claimReward(this.challengeAddress.value, this.userAccount);
	}

}
