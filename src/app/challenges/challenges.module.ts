import { ReactiveFormsModule } from '@angular/forms';
import { ChallengeService } from './challenge.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create/create.component';
import { DescriptionComponent } from './description/description.component';
import { ChallengesListComponent } from './challenges-list/challenges-list.component';
import { JoinComponent } from './join/join.component';
import { NgProgressModule } from 'ngx-progressbar';

@NgModule({
	imports: [
		CommonModule,
		ReactiveFormsModule,
		NgProgressModule
	],
	declarations: [
		CreateComponent,
		DescriptionComponent,
		ChallengesListComponent,
		JoinComponent],
	providers: [ChallengeService],
	exports: [
		CreateComponent,
		DescriptionComponent,
		ChallengesListComponent,
		JoinComponent
	]
})
export class ChallengesModule { }
