import { ChallengesModule } from './challenges/challenges.module';
import { CoreModule } from './core/core.module';
import { KronosModule } from './kronos/kronos.module';
import { NgProgressModule } from 'ngx-progressbar';
import { UtilModule } from './util/util.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		HttpModule,
		UtilModule,
		CoreModule,
		ChallengesModule,
		KronosModule,
		NgProgressModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
