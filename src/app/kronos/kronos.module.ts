import { BrowserModule } from '@angular/platform-browser';
import { UtilModule } from './../util/util.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KronosComponent } from './kronos/kronos.component';

@NgModule({
	imports: [
		CommonModule,
		BrowserModule,
		UtilModule
	],
	declarations: [KronosComponent],
	exports: [KronosComponent]
})
export class KronosModule { }
