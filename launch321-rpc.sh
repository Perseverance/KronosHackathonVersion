#!/bin/bash

# Run this once, but it does not hurt to run it every time
geth --datadir ~/.ethereum/net321 init ./genesis321.json
# Run this every time you start your Geth "42", and add flags here as you need
geth --datadir ~/.ethereum/net321 --networkid 321 --rpc --rpcport 8545 --rpcaddr localhost --rpccorsdomain "*" --rpcapi "eth,net,web3"
